<header>
  <h1>Awesome GP</h1>
</header>

- [Feature selection](#feature-selection)
- [Learning rules](#learning-rules)

## Feature selection

- [Genetic programming for feature construction and selection in classification on high-dimensional data](https://www.researchgate.net/publication/287506504_Genetic_programming_for_feature_construction_and_selection_in_classification_on_high-dimensional_data) (2015)
- [Multi-objective genetic programming for feature extraction and data visualization](https://link.springer.com/article/10.1007%2Fs00500-015-1907-y) (2017)
- [Feature Selection to Improve Generalisation of Genetic Programming for High-Dimensional Symbolic Regression](http://homepages.ecs.vuw.ac.nz/~xuebing/Papers/FinalVersionQi2017.pdf) (2017)
- [Reducing Dimensionality to Improve Search in Semantic Genetic Programming](http://homepages.dcc.ufmg.br/~luizvbo/wp-content/uploads/2017/03/Reducing-Dimensionality-to-Improve-Search-in-Semantic-Genetic-Programming.pdf) (2017)

## Learning rules
- [Learning Fuzzy Rules using Genetic Programming](sci2s.ugr.es/sites/default/files/ficherosPublicaciones/0550_berlanga_deljesus_herrera_GFS05.pdf)

## Cartesian Genetic Programming
- [A Genetic Programming Approach to Designing Convolutional Neural Network Architectures](https://arxiv.org/abs/1704.00764)

## Videogames
### Videogame-playing
- [x] [Multi-Task Learning in Atari Video Games with Emergent Tangled Program Graphs](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&cad=rja&uact=8&ved=0ahUKEwj_u4LdwpTbAhUHUBQKHSxcAQUQFgg0MAE&url=https%3A%2F%2Fweb.cs.dal.ca%2F~mheywood%2FOpenAccess%2Fopen-kelly17b.pdf&usg=AOvVaw1MY2_IrkiiLMbl1kJPMJYl)

## Heap
- [ ] [Evolution Strategies as a Scalable Alternative to Reinforcement Learning](https://arxiv.org/abs/1703.03864) (2017)
- [ ] [Semantic Learning Machine: A Feedforward Neural Network Construction Algorithm Inspired by Geometric Semantic Genetic Programming](https://www.researchgate.net/publication/300543369_Semantic_Learning_Machine_A_Feedforward_Neural_Network_Construction_Algorithm_Inspired_by_Geometric_Semantic_Genetic_Programming) (2015)
- [ ] [Multi-objective Evolutionary Algorithms for Influence Maximization in Social Networks](https://www.researchgate.net/publication/315641713_Multi-objective_Evolutionary_Algorithms_for_Influence_Maximization_in_Social_Networks) (2017)
- [Paul's Online Math Notes](http://tutorial.math.lamar.edu/)
-  [Optimizing Parallel Reduction with Cuda](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwiRn8-uw5TbAhVLVhQKHXIjCngQFggpMAA&url=http%3A%2F%2Fdeveloper.download.nvidia.com%2Fcompute%2Fcuda%2F1.1-Beta%2Fx86_website%2Fprojects%2Freduction%2Fdoc%2Freduction.pdf&usg=AOvVaw0fNP8YpaqLp0v5y55gaOo4)
<!--stackedit_data:
eyJoaXN0b3J5IjpbMjAyMTEzMzIzM119
-->