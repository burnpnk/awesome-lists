- [Lessons Learned Reproducing a Deep Reinforcement Learning Paper](http://amid.fish/reproducing-deep-rl)
- [To Build Truly Intelligent Machines, Teach Them Cause and Effect](https://www.quantamagazine.org/to-build-truly-intelligent-machines-teach-them-cause-and-effect-20180515/)
- [Accelerate Python with Rust](https://developers.redhat.com/blog/2017/11/16/speed-python-using-rust/)
- [Rust Python lib](https://mycognosist.github.io/tutorial-rust-python-lib.html)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTM0NDYxNzc5MCwtMTAzNzYwOTU3XX0=
-->